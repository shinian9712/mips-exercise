# mips exercise

## 介绍
一些MIPS汇编经典练习题

## 题目
- recursion.asm: 递归求解斐波那契数列某项
- prime.asm: 求100以内所有素数
- swap.asm: 交换两个字符串
- reverse.asm: 反转输入的字符串
- multiply.asm: 两数相乘
- dec2bin.asm: 十进制转二进制
- bubbl.asm: 冒泡排序
- tolower.asm: 将输入的字符串大写转小写
- strcmp.asm: 比较输入的字符串
- 3sum.asm: 三数之和
- echo.asm: 回显输入字符
- even_odd.asm: 判断奇偶数
- palindrome.asm: 判断回文数
- reverse_num.asm: 反转整型数字
- toupper.asm: 将输入的字符串小写转大写
- c2f.asm: 摄氏度转华氏度
- strcpy.asm: C语言strcpy函数
- factorial.asm: 递归求阶乘
- permutation.asm: 4个数中取3个数进行全排列
- solve_equation.asm: x + 100 = n^2, x + 100 + 168 = m^2, 求x,m,n
- day_of_year.asm: 求某年某月某日为该年的第几天
- beautiful.asm: 打印漂亮的X图案
- print_c.asm: 打印C图案
- simple_sort.asm: 排序三个数
- 9x9.asm: 打印9x9乘法表
- chessboard.asm: 打印棋盘
- prime2.asm: 打印100～200间的所有素数，每5个一行
- rabbit.asm: 循环求斐波那契数列，每4个一行
- narcissus.asm: 求水仙花数
- decomposition.asm: 因数分解
- grade.asm: 成绩分类
- gcd&lcm.asm: 最大公因数和最小公倍数
- count.asm: 对字符串中不同种类字符进行计数
- drop.asm: 小球自由下落
- perfect_num.asm: 求1000以内完全数
- peach.asm: 猴子吃桃问题
- pingpong.asm: 乒乓比赛问题
- diamond.asm: 打印菱形
- sequence.asm: 数列求和 (2/1，3/2，5/3，8/5，13/8，21/13...)
- sequence2.asm: 数列求和 (1+2!+3!+...+20!)
- reverse_r.asm: 递归反转输入字符
- age.asm: 递归求年龄
- weekday.asm: 根据输入的字符确定星期几
- delete_char.asm: 删除字符串中指定字符
- function.asm: 输出三个hello world
- prime3.asm: 素数筛法求素数
- select_sort.asm: 选择排序
- insert.asm: 在排序数组中插入一个数
- matrix.asm: 计算矩阵主对角线元素之和
- reverse_arr.asm: 反转数组
- heap_sort.asm: 堆排序